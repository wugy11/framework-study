package top.wugy.sharding.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import top.wugy.sharding.SpringBootTransactionTest;
import top.wugy.sharding.transaction.TransactionConfiguration;
import top.wugy.sharding.transaction.XASpringOrderService;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootTransactionTest.class)
@Import(TransactionConfiguration.class)
//@ComponentScan(basePackages = {"top.wugy.sharding.transaction"})
@ActiveProfiles("sharding-database-table")
public class XAOrderServiceTest {

    @Autowired
    private XASpringOrderService orderService;

    @Before
    public void setUp() {
        orderService.init();
    }

    @After
    public void cleanUp() {
        orderService.cleanup();
    }

    @Test
    public void assertInsertSuccess() {
        orderService.insert(10);
        assertThat(orderService.selectAll(), is(10));
    }

    @Test
    public void assertInsertFailed() {
        try {
            orderService.insertFailed(10);
            // CHECKSTYLE:OFF
        } catch (final Exception ignore) {
            // CHECKSTYLE:ON
        }
        assertThat(orderService.selectAll(), is(0));
    }
}