package top.wugy.sharding.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import top.wugy.sharding.SpringBootTransactionTest;
import top.wugy.sharding.transaction.SeataAtSpringOrderService;
import top.wugy.sharding.transaction.TransactionConfiguration;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * wugy 2022/3/12 8:58 下午
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBootTransactionTest.class)
@Import(TransactionConfiguration.class)
//@ComponentScan(basePackages = {"top.wugy.sharding.transaction"})
@ActiveProfiles("sharding-database-table")
public class SeataATOrderServiceTest {

    @Autowired
    private SeataAtSpringOrderService orderService;

    @Before
    public void setUp() {
        orderService.init();
    }

    @After
    public void cleanUp() {
        orderService.cleanup();
    }

    @Test
    public void assertInsertSuccess() {
        orderService.insert(10);
        assertThat(orderService.selectAll(), is(10));
    }
}
