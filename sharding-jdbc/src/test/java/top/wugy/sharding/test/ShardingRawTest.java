package top.wugy.sharding.test;

import org.apache.shardingsphere.sql.parser.api.SQLParserEngine;
import org.apache.shardingsphere.sql.parser.api.SQLVisitorEngine;
import org.apache.shardingsphere.sql.parser.core.ParseContext;
import org.apache.shardingsphere.sql.parser.sql.dialect.statement.mysql.MySQLStatement;
import org.assertj.core.util.Lists;
import org.junit.Test;
import top.wugy.sharding.api.ExampleExecuteTemplate;
import top.wugy.sharding.api.impl.raw.OrderServiceImpl;
import top.wugy.sharding.api.impl.raw.ShadowUserServiceImpl;
import top.wugy.sharding.api.impl.raw.UserServiceImpl;
import top.wugy.sharding.config.EncryptDatabaseConfiguration;
import top.wugy.sharding.factory.DataSourceFactory;
import top.wugy.sharding.kit.ShardingType;
import top.wugy.sharding.repository.impl.ShadowUserRepositoryImpl;
import top.wugy.sharding.repository.impl.UserRepositoryImpl;

import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;

/**
 * 传统Java配置方式
 * <p>
 * wugy 2021/10/17 17:06
 */
public class ShardingRawTest {

    /**
     * 分库
     */
    @Test
    public void testShardingDatabase() throws Exception {
        DataSource dataSource = DataSourceFactory.newInstance(ShardingType.SHARDING_DATABASES);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 分表
     */
    @Test
    public void testShardingTable() throws Exception {
        DataSource dataSource = DataSourceFactory.newInstance(ShardingType.SHARDING_TABLES);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 分库分表
     */
    @Test
    public void testShardingDbAndTable() throws Exception {
        DataSource dataSource = DataSourceFactory.newInstance(ShardingType.SHARDING_DATABASES_AND_TABLES);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 读写分离：运行案例前，需先创建数据库：sharding_write_ds、sharding_read_ds_0、sharding_read_ds_1
     * 再在数据库sharding_read_ds_0、sharding_read_ds_1中分别创建表：t_order、t_order_item，集成springboot无需手动创建
     */
    @Test
    public void testReadwriteSplitting() throws Exception {
        DataSource dataSource = DataSourceFactory.newInstance(ShardingType.READWRITE_SPLITTING);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 数据脱敏
     */
    @Test
    public void testEncrypt() throws Exception {
        DataSource dataSource = new EncryptDatabaseConfiguration().getDataSource();
        ExampleExecuteTemplate.run(new UserServiceImpl(new UserRepositoryImpl(dataSource)));
    }

    @Test
    public void testShadow() throws Exception {
        DataSource dataSource = DataSourceFactory.newInstance(ShardingType.SHADOW);
        ExampleExecuteTemplate.run(new ShadowUserServiceImpl(new ShadowUserRepositoryImpl(dataSource)));
    }

    @Test
    public void testMySQLParserFormat() {
        getMysqlFormatSqlList().forEach(sql -> {
            Properties props = new Properties();
            props.setProperty("parameterized", "false");
            SQLParserEngine parserEngine = new SQLParserEngine("MySQL", false);
            ParseContext parseContext = parserEngine.parse(sql, false);
            SQLVisitorEngine visitorEngine = new SQLVisitorEngine("MySQL", "FORMAT", props);
            String result = visitorEngine.visit(parseContext);
            System.out.println(result);
        });
    }

    private List<String> getMysqlFormatSqlList() {
        return Lists.newArrayList("SELECT age AS b, name AS n FROM table1 JOIN table2 WHERE id = 1 AND name = 'lu';",
                "INSERT INTO user (name, age, status) VALUES ('z', 18, 1);",
                "DELETE FROM user WHERE id = 1;",
                "UPDATE user SET name = 'j' WHERE id = 1;",
                "CREATE TABLE user (id BIGINT(20) PRIMARY KEY AUTO_INCREMENT, name VARCHAR(20), age INT(2), status INT(1));",
                "DROP TABLE user;",
                "ALTER TABLE user CHANGE name name_new VARCHAR(20);",
                "SHOW COLUMNS FROM user;");
    }

    @Test
    public void testMySQLParserStatement() {
        getMysqlParserStatementList().forEach(sql -> {
            SQLParserEngine parserEngine = new SQLParserEngine("MySQL", false);
            ParseContext parseContext = parserEngine.parse(sql, false);
            SQLVisitorEngine visitorEngine = new SQLVisitorEngine("MySQL", "STATEMENT", new Properties());
            MySQLStatement sqlStatement = visitorEngine.visit(parseContext);
            System.out.println(sqlStatement.toString());
        });
    }

    private List<String> getMysqlParserStatementList() {
        return Lists.newArrayList("SELECT t.id, t.name, t.age FROM table1 AS t ORDER BY t.id DESC;",
                "INSERT INTO table1 (name, age) VALUES ('z', 18);",
                "UPDATE table1 SET name = 'j' WHERE id = 1;",
                "DELETE FROM table1 AS t1 WHERE id = 1;",
                "CREATE TABLE table2 (id BIGINT(20) PRIMARY KEY, name VARCHAR(20), age INT(2))",
                "DROP TABLE table1, table2;",
                "ALTER TABLE table1 DROP age;",
                "SHOW COLUMNS FROM table1;");
    }
}
