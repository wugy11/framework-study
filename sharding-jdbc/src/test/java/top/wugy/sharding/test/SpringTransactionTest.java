package top.wugy.sharding.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.wugy.sharding.SpringBootTransactionTest;

import javax.annotation.Resource;

/**
 * wugy 2021/11/21 9:23
 */
@SpringBootTest(classes = SpringBootTransactionTest.class)
@RunWith(SpringRunner.class)
public class SpringTransactionTest {

    @Resource
    private TransactionTestService transactionTestService;

    @Test
    public void testTransaction() {
        transactionTestService.writeFileAndDb();
    }
}
