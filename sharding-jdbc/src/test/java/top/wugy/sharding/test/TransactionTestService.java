package top.wugy.sharding.test;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import top.wugy.sharding.api.ExampleService;

import javax.annotation.Resource;
import java.util.concurrent.FutureTask;

/**
 * wugy 2021/11/21 9:26
 */
@Service
public class TransactionTestService {

    @Resource(name = "userServiceImpl")
    private ExampleService userService;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void writeFileAndDb() {
        FutureTask<Void> futureTask = new FutureTask<>(() -> {
            throw new Exception("test");
        });
        new Thread(futureTask).start();
        try {
//            exampleService.initEnv();
            userService.processSuccess();
            futureTask.get();
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }

}
