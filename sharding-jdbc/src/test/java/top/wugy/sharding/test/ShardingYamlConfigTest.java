package top.wugy.sharding.test;

import org.apache.shardingsphere.infra.hint.HintManager;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import top.wugy.sharding.api.ExampleExecuteTemplate;
import top.wugy.sharding.api.ExampleService;
import top.wugy.sharding.api.impl.raw.OrderServiceImpl;
import top.wugy.sharding.factory.YamlDataSourceFactory;
import top.wugy.sharding.kit.ShardingType;
import top.wugy.sharding.transaction.SeataATOrderService;
import top.wugy.sharding.transaction.XAOrderService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Statement;

/**
 * 基于yaml的配置方式
 * <p>
 * wugy 2021/10/24 17:04
 */
public class ShardingYamlConfigTest {

    /**
     * 分库
     */
    @Test
    public void testShardingDatabase() throws Exception {
        DataSource dataSource = YamlDataSourceFactory.newInstance(ShardingType.SHARDING_DATABASES);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 分表
     */
    @Test
    public void testShardingTable() throws Exception {
        DataSource dataSource = YamlDataSourceFactory.newInstance(ShardingType.SHARDING_TABLES);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 分库分表
     */
    @Test
    public void testShardingDbAndTable() throws Exception {
        DataSource dataSource = YamlDataSourceFactory.newInstance(ShardingType.SHARDING_DATABASES_AND_TABLES);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    /**
     * 读写分离：运行案例前，需先创建数据库：sharding_write_ds、sharding_read_ds_0、sharding_read_ds_1
     * 再在数据库sharding_read_ds_0、sharding_read_ds_1中分别创建表：t_order、t_order_item，集成springboot无需手动创建
     */
    @Test
    public void testReadwriteSplitting() throws Exception {
        DataSource dataSource = YamlDataSourceFactory.newInstance(ShardingType.READWRITE_SPLITTING);
        ExampleExecuteTemplate.run(new OrderServiceImpl(dataSource));
    }

    @Test
    public void testHintDatabaseOnly() throws Exception {
        ShardingType hintType = ShardingType.HINT_DATABASE_ONLY;
        DataSource dataSource = YamlDataSourceFactory.newInstance(hintType);
        ExampleService exampleService = new OrderServiceImpl(dataSource);
        exampleService.initEnv();
        processWithHintValue(dataSource, hintType);
        exampleService.cleanEnv();
    }

    @Test
    public void testHintDatabaseTable() throws Exception {
        ShardingType hintType = ShardingType.HINT_DATABASE_TABLE;
        DataSource dataSource = YamlDataSourceFactory.newInstance(hintType);
        ExampleService exampleService = new OrderServiceImpl(dataSource);
        exampleService.initEnv();
        processWithHintValue(dataSource, hintType);
        exampleService.cleanEnv();
    }

    @Test
    public void testHintSqlComment() throws Exception {
        DataSource dataSource = YamlDataSourceFactory.newInstance(ShardingType.HINT_SQL_COMMENT);
        ExampleService exampleService = new OrderServiceImpl(dataSource);
        exampleService.initEnv();
        processWithHintCommentValue(dataSource);
        exampleService.cleanEnv();
    }

    @Test
    public void testHintMasterSalve() throws Exception {
        ShardingType hintType = ShardingType.HINT_MASTER_SALVE;
        DataSource dataSource = YamlDataSourceFactory.newInstance(hintType);
        ExampleService exampleService = new OrderServiceImpl(dataSource);
        exampleService.initEnv();
        processWithHintValue(dataSource, hintType);
        exampleService.cleanEnv();
    }

    @Test
    public void testXATransaction() throws Exception {
        XAOrderService orderService = new XAOrderService("/META-INF/yaml/sharding-database-table.yaml");
        orderService.init();

        orderService.insert();
        Assert.assertThat(orderService.selectAll(), CoreMatchers.is(10));
        orderService.cleanup();
    }

    @Test
    public void testSeataTransaction() throws Exception {
        SeataATOrderService orderService = new SeataATOrderService("/META-INF/yaml/sharding-database-table.yaml");
        orderService.init();

        orderService.insert();
        Assert.assertThat(orderService.selectAll(), CoreMatchers.is(10));
        orderService.cleanup();
    }

    private static void processWithHintValue(DataSource dataSource, ShardingType hintType) throws Exception {
        try (HintManager hintManager = HintManager.getInstance();
             Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            setHintValue(hintManager, hintType);
            statement.execute("select * from t_order");
            statement.execute("SELECT i.* FROM t_order o, t_order_item i WHERE o.order_id = i.order_id");
            statement.execute("select * from t_order_item");
            statement.execute("INSERT INTO t_order (user_id, address_id, status) VALUES (1, 1, 'init')");
        }
    }

    private static void processWithHintCommentValue(final DataSource dataSource) throws Exception {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("/* sql hint: dataSourceName=ds_1 */select * from t_order");
            statement.execute("/* sql hint: dataSourceName=ds_1 */SELECT i.* FROM t_order o, t_order_item i WHERE o.order_id = i.order_id");
            statement.execute("/* sql hint: dataSourceName=ds_1 */select * from t_order_item");
            statement.execute("/* sql hint: dataSourceName=ds_1 */INSERT INTO t_order (user_id, address_id, status) VALUES (1, 1, 'init')");
        }
    }

    private static void setHintValue(HintManager hintManager, ShardingType hintType) {
        switch (hintType) {
            case HINT_DATABASE_TABLE:
                hintManager.addDatabaseShardingValue("t_order", 1L);
                hintManager.addTableShardingValue("t_order", 1L);
                return;
            case HINT_DATABASE_ONLY:
                hintManager.setDatabaseShardingValue(1L);
                return;
//            case HINT_MASTER_SALVE:
//                hintManager.setMasterRouteOnly();
//                return;
            default:
                throw new UnsupportedOperationException("unsupported type");
        }
    }
}
