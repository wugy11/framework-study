package top.wugy.sharding;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * wugy 2021/11/21 9:58
 */
@EnableTransactionManagement
@MapperScan(basePackages = "top.wugy.sharding.transaction")
@SpringBootApplication
        //(exclude = {JtaAutoConfiguration.class, SpringBootConfiguration.class})
public class SpringBootTransactionTest {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTransactionTest.class, args);
    }

}
