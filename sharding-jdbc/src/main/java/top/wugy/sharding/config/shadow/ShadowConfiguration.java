package top.wugy.sharding.config.shadow;

import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.infra.config.RuleConfiguration;
import org.apache.shardingsphere.shadow.api.config.ShadowRuleConfiguration;
import org.apache.shardingsphere.shadow.api.config.datasource.ShadowDataSourceConfiguration;
import org.apache.shardingsphere.shadow.api.config.table.ShadowTableConfiguration;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * wugy 2022/2/12 8:30 上午
 */
public class ShadowConfiguration extends AbstractShadowConfiguration {

    @Override
    public DataSource getDataSource() throws Exception {
        Map<String, DataSource> dataSourceMap = createDataSourceMap();
        return ShardingSphereDataSourceFactory.createDataSource(dataSourceMap, createRuleConfiguration(),
                createShardingProperty());
    }

    private Collection<RuleConfiguration> createRuleConfiguration() {
        Collection<RuleConfiguration> ruleConfigurations = new LinkedList<>();

        ShadowRuleConfiguration shadowRule = new ShadowRuleConfiguration();
        shadowRule.setEnable(true);
        shadowRule.setShadowAlgorithms(createShadowAlgorithmConfigurations());
        shadowRule.setDataSources(createShadowDataSources());
        shadowRule.setTables(createShadowTables());
        ruleConfigurations.add(shadowRule);

        return ruleConfigurations;
    }

    private Map<String, ShadowTableConfiguration> createShadowTables() {
        Map<String, ShadowTableConfiguration> result = new LinkedHashMap<>();
        result.put("t_user", new ShadowTableConfiguration(createDataSourceNames(), createShadowAlgorithm()));
        return result;
    }

    private Collection<String> createDataSourceNames() {
        Collection<String> result = new LinkedList<>();
        result.add("shadow-data-source");
        return result;
    }

    private Map<String, ShadowDataSourceConfiguration> createShadowDataSources() {
        Map<String, ShadowDataSourceConfiguration> result = new LinkedHashMap<>();
        result.put("shadow-data-source", new ShadowDataSourceConfiguration("ds", "shadow-ds"));
        return result;
    }
}
