package top.wugy.sharding.config;

import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.encrypt.api.config.EncryptRuleConfiguration;
import org.apache.shardingsphere.encrypt.api.config.rule.EncryptColumnRuleConfiguration;
import org.apache.shardingsphere.encrypt.api.config.rule.EncryptTableRuleConfiguration;
import org.apache.shardingsphere.infra.config.algorithm.ShardingSphereAlgorithmConfiguration;
import top.wugy.sharding.api.ExampleDataSource;
import top.wugy.sharding.kit.ShardingKit;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2021/11/27 13:04
 */
public class EncryptDatabaseConfiguration implements ExampleDataSource {

    @Override
    public DataSource getDataSource() {
        Properties props = new Properties();
        props.setProperty("aes-key-value", "123456");
        EncryptColumnRuleConfiguration columnConfigAes = new EncryptColumnRuleConfiguration("user_name",
                "user_name", "", "user_name_plain", "name_encryptor");
        EncryptColumnRuleConfiguration columnConfigTest = new EncryptColumnRuleConfiguration("pwd", "pwd",
                "assisted_query_pwd", "", "pwd_encryptor");
        EncryptTableRuleConfiguration encryptTableRuleConfig = new EncryptTableRuleConfiguration("t_user",
                Arrays.asList(columnConfigAes, columnConfigTest), null);
        Map<String, ShardingSphereAlgorithmConfiguration> encryptAlgorithmConfigs = new LinkedHashMap<>(2, 1);
        encryptAlgorithmConfigs.put("name_encryptor", new ShardingSphereAlgorithmConfiguration("AES", props));
        encryptAlgorithmConfigs.put("pwd_encryptor", new ShardingSphereAlgorithmConfiguration("assistedTest", props));
        EncryptRuleConfiguration encryptRuleConfig = new EncryptRuleConfiguration(Collections.singleton(encryptTableRuleConfig),
                encryptAlgorithmConfigs);
        try {
            return ShardingSphereDataSourceFactory.createDataSource(ShardingKit.createDataSource("sharding_ds"),
                    Collections.singleton(encryptRuleConfig), props);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
