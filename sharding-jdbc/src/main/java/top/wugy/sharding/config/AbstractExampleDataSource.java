package top.wugy.sharding.config;

import com.google.common.collect.Maps;
import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.infra.config.mode.ModeConfiguration;
import org.apache.shardingsphere.mode.repository.standalone.StandalonePersistRepositoryConfiguration;
import org.apache.shardingsphere.sharding.api.config.ShardingRuleConfiguration;
import top.wugy.sharding.api.ExampleDataSource;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2021/10/23 18:27
 */
public abstract class AbstractExampleDataSource implements ExampleDataSource {

    @Override
    public DataSource getDataSource() throws Exception {
        ShardingRuleConfiguration ruleConfiguration = new ShardingRuleConfiguration();
        configShardingRule(ruleConfiguration);

        Map<String, DataSource> dataSourceMap = Maps.newHashMap();
        configDataSourceMap(dataSourceMap);
        return ShardingSphereDataSourceFactory.createDataSource(createModeConfiguration(), dataSourceMap,
                Collections.singleton(ruleConfiguration), new Properties());
    }

    private ModeConfiguration createModeConfiguration() {
        return new ModeConfiguration("Standalone", new StandalonePersistRepositoryConfiguration("File", new Properties()), true);
    }

    protected abstract void configDataSourceMap(Map<String, DataSource> dataSourceMap);

    protected abstract void configShardingRule(ShardingRuleConfiguration ruleConfiguration);

    protected Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("worker-id", "123");
        return properties;
    }
}
