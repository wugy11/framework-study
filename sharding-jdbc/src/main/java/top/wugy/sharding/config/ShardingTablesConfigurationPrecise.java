package top.wugy.sharding.config;

import org.apache.shardingsphere.infra.config.algorithm.ShardingSphereAlgorithmConfiguration;
import org.apache.shardingsphere.sharding.api.config.ShardingRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.rule.ShardingTableRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.keygen.KeyGenerateStrategyConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.sharding.StandardShardingStrategyConfiguration;
import top.wugy.sharding.kit.ShardingKit;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2021/10/23 18:23
 */
public class ShardingTablesConfigurationPrecise extends AbstractExampleDataSource {

    @Override
    protected void configDataSourceMap(Map<String, DataSource> dataSourceMap) {
        dataSourceMap.put("sharding_ds", ShardingKit.createDataSource("sharding_ds"));
    }

    @Override
    protected void configShardingRule(ShardingRuleConfiguration ruleConfiguration) {
        ruleConfiguration.getTables().add(orderTableRuleConfig());
        ruleConfiguration.getTables().add(orderItemTableRuleConfig());
        ruleConfiguration.getBindingTableGroups().add("t_order, t_order_item");
        ruleConfiguration.getBroadcastTables().add("t_address");
        ruleConfiguration.setDefaultTableShardingStrategy(
                new StandardShardingStrategyConfiguration("order_id", "standard_test_tbl"));
        ruleConfiguration.getShardingAlgorithms().put("standard_test_tbl",
                new ShardingSphereAlgorithmConfiguration("standard_test_tbl", new Properties()));
        ruleConfiguration.getKeyGenerators().put("snowflake", new ShardingSphereAlgorithmConfiguration("snowflake",
                getProperties()));
    }

    private ShardingTableRuleConfiguration orderItemTableRuleConfig() {
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("t_order_item",
                "sharding_ds.t_order_item_${[0, 1]}");
        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("order_item_id", "snowflake"));
        return result;
    }

    private ShardingTableRuleConfiguration orderTableRuleConfig() {
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("t_order",
                "sharding_ds.t_order_${[0, 1]}");
        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("order_id", "snowflake"));
        return result;
    }

}
