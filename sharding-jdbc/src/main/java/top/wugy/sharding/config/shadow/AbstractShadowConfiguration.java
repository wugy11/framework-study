package top.wugy.sharding.config.shadow;

import com.google.common.collect.Maps;
import org.apache.shardingsphere.infra.config.algorithm.ShardingSphereAlgorithmConfiguration;
import org.apache.shardingsphere.infra.config.properties.ConfigurationPropertyKey;
import top.wugy.sharding.api.ExampleDataSource;
import top.wugy.sharding.kit.ShardingKit;

import javax.sql.DataSource;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2022/2/12 7:40 上午
 */
public abstract class AbstractShadowConfiguration implements ExampleDataSource {

    protected Map<String, DataSource> createDataSourceMap() {
        Map<String, DataSource> datasourceMap = Maps.newHashMap();
        datasourceMap.put("sharding_ds", ShardingKit.createDataSource("sharding_ds"));
        datasourceMap.put("sharding_ds_shadow", ShardingKit.createDataSource("sharding_ds_shadow"));
        return datasourceMap;
    }

    protected Properties createShardingProperty() {
        Properties properties = new Properties();
        properties.setProperty(ConfigurationPropertyKey.SQL_SHOW.getKey(), "true");
        properties.setProperty(ConfigurationPropertyKey.SQL_COMMENT_PARSE_ENABLED.getKey(), "true");
        return properties;
    }

    protected Collection<String> createShadowAlgorithm() {
        Collection<String> result = new LinkedList<>();
        result.add("user-id-insert-match-algorithm");
        result.add("user-id-delete-match-algorithm");
        result.add("user-id-select-match-algorithm");
        result.add("simple-note-algorithm");
        return result;
    }

    protected Map<String, ShardingSphereAlgorithmConfiguration> createShadowAlgorithmConfigurations() {
        Map<String, ShardingSphereAlgorithmConfiguration> result = new LinkedHashMap<>();
        Properties userIdInsertProps = new Properties();
        userIdInsertProps.setProperty("operation", "insert");
        userIdInsertProps.setProperty("column", "user_type");
        userIdInsertProps.setProperty("value", "1");
        result.put("user-id-insert-match-algorithm", new ShardingSphereAlgorithmConfiguration("COLUMN_VALUE_MATCH",
                userIdInsertProps));

        Properties userIdDeleteProps = new Properties();
        userIdDeleteProps.setProperty("operation", "delete");
        userIdDeleteProps.setProperty("column", "user_type");
        userIdDeleteProps.setProperty("value", "1");
        result.put("user-id-delete-match-algorithm", new ShardingSphereAlgorithmConfiguration("COLUMN_VALUE_MATCH",
                userIdDeleteProps));

        Properties userIdSelectProps = new Properties();
        userIdSelectProps.setProperty("operation", "select");
        userIdSelectProps.setProperty("column", "user_type");
        userIdSelectProps.setProperty("value", "1");
        result.put("user-id-select-match-algorithm", new ShardingSphereAlgorithmConfiguration("COLUMN_VALUE_MATCH",
                userIdSelectProps));

        Properties noteAlgorithmProps = new Properties();
        noteAlgorithmProps.setProperty("shadow", "true");
        noteAlgorithmProps.setProperty("foo", "bar");
        result.put("simple-note-algorithm", new ShardingSphereAlgorithmConfiguration("SIMPLE_NOTE", noteAlgorithmProps));
        return result;
    }
}
