package top.wugy.sharding.config;

import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.readwritesplitting.api.ReadwriteSplittingRuleConfiguration;
import org.apache.shardingsphere.readwritesplitting.api.rule.ReadwriteSplittingDataSourceRuleConfiguration;
import top.wugy.sharding.api.ExampleDataSource;
import top.wugy.sharding.kit.ShardingKit;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2022/2/7 8:30 下午
 */
public class ReadwriteSplittingConfiguration implements ExampleDataSource {

    @Override
    public DataSource getDataSource() throws Exception {
        ReadwriteSplittingDataSourceRuleConfiguration dataSourceConfig =
                new ReadwriteSplittingDataSourceRuleConfiguration("sharding_read_query_ds", "",
                        "sharding_write_ds", Arrays.asList("sharding_read_ds_0", "sharding_read_ds_1"),
                        null);
        ReadwriteSplittingRuleConfiguration ruleConfig =
                new ReadwriteSplittingRuleConfiguration(Collections.singleton(dataSourceConfig), Collections.emptyMap());
        return ShardingSphereDataSourceFactory.createDataSource(createDataSourceMap(), Collections.singleton(ruleConfig),
                new Properties());
    }

    private Map<String, DataSource> createDataSourceMap() {
        Map<String, DataSource> result = new HashMap<>(3, 1);
        result.put("sharding_write_ds", ShardingKit.createDataSource("sharding_write_ds"));
        result.put("sharding_read_ds_0", ShardingKit.createDataSource("sharding_read_ds_0"));
        result.put("sharding_read_ds_1", ShardingKit.createDataSource("sharding_read_ds_1"));
        return result;
    }
}
