package top.wugy.sharding.config;

import org.apache.shardingsphere.infra.config.algorithm.ShardingSphereAlgorithmConfiguration;
import org.apache.shardingsphere.sharding.api.config.ShardingRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.rule.ShardingTableRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.keygen.KeyGenerateStrategyConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.sharding.StandardShardingStrategyConfiguration;
import top.wugy.sharding.kit.ShardingKit;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2021/10/23 18:23
 */
public class ShardingDatabasesAndTablesConfigurationPrecise extends AbstractExampleDataSource {

    @Override
    protected void configDataSourceMap(Map<String, DataSource> dataSourceMap) {
        dataSourceMap.put("sharding_ds_0", ShardingKit.createDataSource("sharding_ds_0"));
        dataSourceMap.put("sharding_ds_1", ShardingKit.createDataSource("sharding_ds_1"));
    }

    @Override
    protected void configShardingRule(ShardingRuleConfiguration ruleConfiguration) {
        ruleConfiguration.getTables().add(orderTableRuleConfig());
        ruleConfiguration.getTables().add(orderItemTableRuleConfig());
        ruleConfiguration.getBindingTableGroups().add("t_order, t_order_item");
        ruleConfiguration.getBroadcastTables().add("t_address");
        ruleConfiguration.setDefaultDatabaseShardingStrategy(new StandardShardingStrategyConfiguration("user_id",
                "inline"));
        ruleConfiguration.setDefaultTableShardingStrategy(new StandardShardingStrategyConfiguration("order_id",
                "standard_test_tbl"));
        Properties props = new Properties();
        props.setProperty("algorithm-expression", "sharding_ds_${user_id % 2}");
        ruleConfiguration.getShardingAlgorithms().put("inline", new ShardingSphereAlgorithmConfiguration("inline", props));
        ruleConfiguration.getShardingAlgorithms().put("standard_test_tbl",
                new ShardingSphereAlgorithmConfiguration("standard_test_tbl", new Properties()));
        ruleConfiguration.getKeyGenerators().put("snowflake",
                new ShardingSphereAlgorithmConfiguration("snowflake", getProperties()));
    }

    private ShardingTableRuleConfiguration orderItemTableRuleConfig() {
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("t_order_item",
                "sharding_ds_${0..1}.t_order_item_${[0, 1]}");
        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("order_item_id", "snowflake"));
        return result;
    }

    private ShardingTableRuleConfiguration orderTableRuleConfig() {
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("t_order",
                "sharding_ds_${0..1}.t_order_${[0, 1]}");
        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("order_id", "snowflake"));
        return result;
    }
}
