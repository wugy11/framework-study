package top.wugy.sharding.config;

import org.apache.shardingsphere.infra.config.algorithm.ShardingSphereAlgorithmConfiguration;
import org.apache.shardingsphere.sharding.api.config.ShardingRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.rule.ShardingTableRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.keygen.KeyGenerateStrategyConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.sharding.StandardShardingStrategyConfiguration;
import top.wugy.sharding.kit.ShardingKit;

import javax.sql.DataSource;
import java.util.Map;
import java.util.Properties;

/**
 * wugy 2021/10/17 17:39
 */
public class ShardingDatabasesConfigurationPrecise extends AbstractExampleDataSource {

    private ShardingTableRuleConfiguration orderTableRuleConfig() {
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("t_order");
        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("order_id", "snowflake"));
        return result;
    }

    private ShardingTableRuleConfiguration orderItemTableRuleConfig() {
        ShardingTableRuleConfiguration result = new ShardingTableRuleConfiguration("t_order_item");
        result.setKeyGenerateStrategy(new KeyGenerateStrategyConfiguration("order_item_id", "snowflake"));
        return result;
    }

    @Override
    protected void configDataSourceMap(Map<String, DataSource> dataSourceMap) {
        dataSourceMap.put("sharding_ds_0", ShardingKit.createDataSource("sharding_ds_0"));
        dataSourceMap.put("sharding_ds_1", ShardingKit.createDataSource("sharding_ds_1"));
    }

    @Override
    protected void configShardingRule(ShardingRuleConfiguration ruleConfiguration) {
        ruleConfiguration.getTables().add(orderTableRuleConfig());
        ruleConfiguration.getTables().add(orderItemTableRuleConfig());
        // 广播表：sharding_ds_0、sharding_ds_1两个库落的数据一致
        ruleConfiguration.getBroadcastTables().add("t_address");
        ruleConfiguration.setDefaultDatabaseShardingStrategy(
                new StandardShardingStrategyConfiguration("user_id", "inline"));
        Properties props = new Properties();
        // userId为奇数时落入sharding_ds_1库，为偶数时落入sharding_ds_0
        props.setProperty("algorithm-expression", "sharding_ds_${user_id % 2}");
        ruleConfiguration.getShardingAlgorithms().put("inline",
                new ShardingSphereAlgorithmConfiguration("inline", props));
        ruleConfiguration.getKeyGenerators().put("snowflake",
                new ShardingSphereAlgorithmConfiguration("snowflake", getProperties()));
    }

}
