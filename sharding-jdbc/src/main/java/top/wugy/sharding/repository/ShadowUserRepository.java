package top.wugy.sharding.repository;

import top.wugy.sharding.entity.ShadowUser;

/**
 * wugy 2022/2/12 8:56 上午
 */
public interface ShadowUserRepository extends CommonRepository<ShadowUser>{
}
