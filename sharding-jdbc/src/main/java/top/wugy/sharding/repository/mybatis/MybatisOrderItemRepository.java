package top.wugy.sharding.repository.mybatis;

import org.apache.ibatis.annotations.Mapper;
import top.wugy.sharding.repository.OrderItemRepository;

@Mapper
public interface MybatisOrderItemRepository extends OrderItemRepository {
}
