package top.wugy.sharding.repository.mybatis;

import org.apache.ibatis.annotations.Mapper;
import top.wugy.sharding.repository.AddressRepository;

@Mapper
public interface MybatisAddressRepository extends AddressRepository {

}
