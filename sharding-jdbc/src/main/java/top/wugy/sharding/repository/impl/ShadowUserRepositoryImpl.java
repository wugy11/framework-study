package top.wugy.sharding.repository.impl;

import top.wugy.sharding.entity.ShadowUser;
import top.wugy.sharding.repository.AbstractCommonRepository;
import top.wugy.sharding.repository.ShadowUserRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * wugy 2022/2/12 9:01 上午
 */
public class ShadowUserRepositoryImpl extends AbstractCommonRepository<ShadowUser> implements ShadowUserRepository {

    private static final String SQL_NOTE = "/*shadow:true,foo:bar*/";

    public ShadowUserRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public void createTableIfNotExist() throws Exception {
        String sql = createTableSql();

        executeUpdate(sql);
        executeUpdate(sql + SQL_NOTE);
    }

    @Override
    public void dropTable() throws Exception {
        String sql = dropTableSql();

        executeUpdate(sql);
        executeUpdate(sql + SQL_NOTE);
    }

    @Override
    public void truncateTable() throws Exception {
        String sql = truncateTableSql();

        executeUpdate(sql);
        executeUpdate(sql + SQL_NOTE);
    }

    @Override
    public void delete(long id) throws Exception {
        String sql = preparedDeleteSql();

        deleteUser(sql, id, (int) (id % 2));
        deleteUser(sql, id, (int) (id % 2));
    }

    private void deleteUser(final String sql, final Long id, final int userType) throws Exception {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, id);
            preparedStatement.setInt(2, userType);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    protected String createTableSql() {
        return "CREATE TABLE IF NOT EXISTS t_user (user_id INT NOT NULL AUTO_INCREMENT, user_type INT(11), user_name VARCHAR(200), pwd VARCHAR(200), PRIMARY KEY (user_id))";
    }

    @Override
    protected String dropTableSql() {
        return "DROP TABLE IF EXISTS t_user;";
    }

    @Override
    protected String truncateTableSql() {
        return "TRUNCATE TABLE t_user";
    }

    @Override
    protected String preparedDeleteSql() {
        return "DELETE FROM t_user WHERE user_id = ? AND user_type = ?";
    }

    @Override
    protected String selectAllSql() {
        return "SELECT * FROM t_user";
    }

    @Override
    protected ShadowUser buildEntity(ResultSet resultSet) throws Exception {
        ShadowUser user = new ShadowUser();
        user.setUserId(resultSet.getInt("user_id"));
        user.setUserType(resultSet.getInt("user_type"));
        user.setUserName(resultSet.getString("user_name"));
        user.setPwd(resultSet.getString("pwd"));
        return user;
    }

    @Override
    public long insert(ShadowUser entity) throws Exception {
        String sql = "INSERT INTO t_user (user_id, user_type, user_name, pwd) VALUES (?, ?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, entity.getUserId());
            preparedStatement.setInt(2, entity.getUserType());
            preparedStatement.setString(3, entity.getUserName());
            preparedStatement.setString(4, entity.getPwd());
            preparedStatement.executeUpdate();
        }
        return entity.getUserId();
    }
}
