package top.wugy.sharding.repository.impl;

import top.wugy.sharding.entity.User;
import top.wugy.sharding.repository.AbstractCommonRepository;
import top.wugy.sharding.repository.UserRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * wugy 2021/10/17 20:25
 */
public class UserRepositoryImpl extends AbstractCommonRepository<User> implements UserRepository {

    public UserRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public long insert(final User entity) throws Exception {
        String sql = "INSERT INTO t_user (user_id, user_name, pwd) VALUES (?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getUserId());
            preparedStatement.setString(2, entity.getUserName());
            preparedStatement.setString(3, entity.getPwd());
            preparedStatement.executeUpdate();
        }
        return entity.getUserId();
    }

    @Override
    protected String createTableSql() {
        return "CREATE TABLE IF NOT EXISTS t_user "
                + "(user_id BIGINT NOT NULL AUTO_INCREMENT, user_name VARCHAR(200), pwd VARCHAR(200),  PRIMARY KEY (user_id))";
    }

    @Override
    protected String dropTableSql() {
        return "DROP TABLE t_user";
    }

    @Override
    protected String truncateTableSql() {
        return "TRUNCATE TABLE t_user";
    }

    @Override
    protected String preparedDeleteSql() {
        return "DELETE FROM t_user WHERE user_id=?";
    }

    @Override
    protected String selectAllSql() {
        return "SELECT * FROM t_user";
    }

    @Override
    protected User buildEntity(ResultSet resultSet) throws Exception {
        User user = new User();
        user.setUserId(resultSet.getInt("user_id"));
        user.setUserName(resultSet.getString("user_name"));
        user.setPwd(resultSet.getString("pwd"));
        return user;
    }

}
