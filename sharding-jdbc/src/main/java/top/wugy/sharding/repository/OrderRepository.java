package top.wugy.sharding.repository;

import top.wugy.sharding.entity.Order;

/**
 * wugy 2021/10/17 18:40
 */
public interface OrderRepository extends CommonRepository<Order> {

}
