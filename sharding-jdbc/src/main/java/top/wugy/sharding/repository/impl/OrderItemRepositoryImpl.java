package top.wugy.sharding.repository.impl;

import top.wugy.sharding.entity.OrderItem;
import top.wugy.sharding.repository.AbstractCommonRepository;
import top.wugy.sharding.repository.OrderItemRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * wugy 2021/10/17 20:23
 */
public class OrderItemRepositoryImpl extends AbstractCommonRepository<OrderItem> implements OrderItemRepository {

    public OrderItemRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public long insert(final OrderItem orderItem) throws Exception {
        String sql = "INSERT INTO t_order_item (order_id, user_id, status) VALUES (?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setLong(1, orderItem.getOrderId());
            preparedStatement.setInt(2, orderItem.getUserId());
            preparedStatement.setString(3, orderItem.getStatus());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    orderItem.setOrderItemId(resultSet.getLong(1));
                }
            }
        }
        return orderItem.getOrderItemId();
    }

    @Override
    protected String createTableSql() {
        return "CREATE TABLE IF NOT EXISTS t_order_item "
                + "(order_item_id BIGINT NOT NULL AUTO_INCREMENT, order_id BIGINT NOT NULL, user_id INT NOT NULL, "
                + "status VARCHAR(50), PRIMARY KEY (order_item_id))";
    }

    @Override
    protected String dropTableSql() {
        return "DROP TABLE t_order_item";
    }

    @Override
    protected String truncateTableSql() {
        return "TRUNCATE TABLE t_order_item";
    }

    @Override
    protected String preparedDeleteSql() {
        return "DELETE FROM t_order_item WHERE order_id=?";
    }

    @Override
    protected String selectAllSql() {
        return "SELECT * FROM t_order_item";
    }

    @Override
    protected OrderItem buildEntity(ResultSet resultSet) throws Exception {
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderItemId(resultSet.getLong(1));
        orderItem.setOrderId(resultSet.getLong(2));
        orderItem.setUserId(resultSet.getInt(3));
        orderItem.setStatus(resultSet.getString(4));
        return orderItem;
    }

}
