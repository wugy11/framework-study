package top.wugy.sharding.repository.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import top.wugy.sharding.entity.UserEntity;
import top.wugy.sharding.repository.UserRepository;

import java.util.List;

@Mapper
public interface MybatisUserRepository extends UserRepository {

    List<UserEntity> getUserAndAddr(@Param("username") String username);
}
