package top.wugy.sharding.repository;

import java.util.List;

/**
 * wugy 2021/10/17 18:29
 */
public interface CommonRepository<T> {

    void createTableIfNotExist() throws Exception;

    void dropTable() throws Exception;

    void truncateTable() throws Exception;

    long insert(T entity) throws Exception;

    void delete(long primaryKey) throws Exception;

    List<T> selectAll() throws Exception;
}
