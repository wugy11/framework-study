package top.wugy.sharding.repository.impl;

import top.wugy.sharding.entity.Order;
import top.wugy.sharding.repository.AbstractCommonRepository;
import top.wugy.sharding.repository.OrderRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * wugy 2021/10/17 18:52
 */
public class OrderRepositoryImpl extends AbstractCommonRepository<Order> implements OrderRepository {

    public OrderRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public long insert(Order order) throws Exception {
        String sql = "INSERT INTO t_order (user_id, address_id, status) VALUES (?, ?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, order.getUserId());
            preparedStatement.setLong(2, order.getAddressId());
            preparedStatement.setString(3, order.getStatus());
            preparedStatement.executeUpdate();
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    order.setOrderId(resultSet.getLong(1));
                }
            }
        }
        return order.getOrderId();

    }

    @Override
    protected String createTableSql() {
        return "CREATE TABLE IF NOT EXISTS t_order (order_id BIGINT NOT NULL AUTO_INCREMENT, user_id BIGINT NOT NULL, " +
                "address_id BIGINT NOT NULL, status VARCHAR(50), PRIMARY KEY (order_id))";
    }

    @Override
    protected String dropTableSql() {
        return "DROP TABLE t_order";
    }

    @Override
    protected String truncateTableSql() {
        return "TRUNCATE TABLE t_order";
    }

    @Override
    protected String preparedDeleteSql() {
        return "DELETE FROM t_order WHERE order_id=?";
    }

    @Override
    protected String selectAllSql() {
        return "SELECT * FROM t_order";
    }

    @Override
    protected Order buildEntity(ResultSet resultSet) throws Exception {
        Order order = new Order();
        order.setOrderId(resultSet.getLong(1));
        order.setUserId(resultSet.getInt(2));
        order.setAddressId(resultSet.getLong(3));
        order.setStatus(resultSet.getString(4));
        return order;
    }
}
