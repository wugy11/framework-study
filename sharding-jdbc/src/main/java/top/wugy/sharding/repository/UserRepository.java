package top.wugy.sharding.repository;

import top.wugy.sharding.entity.User;

/**
 * wugy 2021/10/17 18:41
 */
public interface UserRepository extends CommonRepository<User> {
}
