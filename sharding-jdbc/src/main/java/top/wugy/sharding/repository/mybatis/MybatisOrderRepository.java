package top.wugy.sharding.repository.mybatis;

import org.apache.ibatis.annotations.Mapper;
import top.wugy.sharding.repository.OrderRepository;

@Mapper
public interface MybatisOrderRepository extends OrderRepository {
}
