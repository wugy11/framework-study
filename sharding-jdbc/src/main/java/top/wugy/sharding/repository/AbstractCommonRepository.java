package top.wugy.sharding.repository;

import com.google.common.collect.Lists;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * wugy 2021/10/24 11:06
 */
public abstract class AbstractCommonRepository<T> implements CommonRepository<T> {

    protected final DataSource dataSource;

    protected AbstractCommonRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void createTableIfNotExist() throws Exception {
        executeUpdate(createTableSql());
    }

    @Override
    public void dropTable() throws Exception {
        executeUpdate(dropTableSql());
    }

    @Override
    public void truncateTable() throws Exception {
        executeUpdate(truncateTableSql());
    }

    @Override
    public void delete(long primaryKey) throws Exception {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(preparedDeleteSql())) {
            preparedStatement.setLong(1, primaryKey);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public final List<T> selectAll() throws Exception {
        List<T> result = Lists.newLinkedList();
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(selectAllSql());
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                result.add(buildEntity(resultSet));
            }
        }
        return result;
    }

    protected final void executeUpdate(String sql) throws Exception {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    protected abstract String createTableSql();

    protected abstract String dropTableSql();

    protected abstract String truncateTableSql();

    protected abstract String preparedDeleteSql();

    protected abstract String selectAllSql();

    protected abstract T buildEntity(ResultSet resultSet) throws Exception;
}
