package top.wugy.sharding.repository;

import top.wugy.sharding.entity.OrderItem;

/**
 * wugy 2021/10/17 18:41
 */
public interface OrderItemRepository extends CommonRepository<OrderItem> {
}
