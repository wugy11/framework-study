package top.wugy.sharding.repository.impl;

import top.wugy.sharding.entity.Address;
import top.wugy.sharding.repository.AbstractCommonRepository;
import top.wugy.sharding.repository.AddressRepository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * wugy 2021/10/17 20:21
 */
public class AddressRepositoryImpl extends AbstractCommonRepository<Address> implements AddressRepository {

    public AddressRepositoryImpl(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public long insert(final Address entity) throws Exception {
        String sql = "INSERT INTO t_address (address_id, address_name) VALUES (?, ?)";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, entity.getAddressId());
            preparedStatement.setString(2, entity.getAddressName());
            preparedStatement.executeUpdate();
        }
        return entity.getAddressId();
    }

    @Override
    protected String createTableSql() {
        return "CREATE TABLE IF NOT EXISTS t_address (address_id BIGINT NOT NULL, address_name VARCHAR(100) NOT NULL, PRIMARY KEY (address_id))";
    }

    @Override
    protected String dropTableSql() {
        return "DROP TABLE t_address";
    }

    @Override
    protected String truncateTableSql() {
        return "TRUNCATE TABLE t_address";
    }

    @Override
    protected String preparedDeleteSql() {
        return "DELETE FROM t_address WHERE address_id=?";
    }

    @Override
    protected String selectAllSql() {
        return "SELECT * FROM t_address";
    }

    @Override
    protected Address buildEntity(ResultSet resultSet) throws Exception {
        Address address = new Address();
        address.setAddressId(resultSet.getLong(1));
        address.setAddressName(resultSet.getString(2));
        return address;
    }
}
