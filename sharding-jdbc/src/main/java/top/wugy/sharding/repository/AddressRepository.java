package top.wugy.sharding.repository;

import top.wugy.sharding.entity.Address;

/**
 * wugy 2021/10/17 18:42
 */
public interface AddressRepository extends CommonRepository<Address> {
}
