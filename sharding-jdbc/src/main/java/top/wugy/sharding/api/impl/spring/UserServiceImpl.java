package top.wugy.sharding.api.impl.spring;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.wugy.sharding.api.ExampleService;
import top.wugy.sharding.entity.User;
import top.wugy.sharding.kit.ShardingKit;
import top.wugy.sharding.repository.UserRepository;

import java.util.List;

/**
 * wugy 2021/10/17 20:34
 */
@Service
public class UserServiceImpl implements ExampleService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void initEnv() throws Exception {
        userRepository.createTableIfNotExist();
        userRepository.truncateTable();
    }

    @Override
    public void cleanEnv() throws Exception {
        userRepository.dropTable();
    }

    @Override
    @Transactional
    public void processSuccess() throws Exception {
        ShardingKit.print("Process Success Begin");
        List<Long> userIds = insertData();
        printData();
        deleteData(userIds);
        printData();
        ShardingKit.print("Process Success Finish");
    }

    @Override
    @Transactional
    public void processFailure() throws Exception {
        ShardingKit.print("Process Failure Begin");
        insertData();
        ShardingKit.print("Process Failure Finish");
        throw new RuntimeException("Exception occur for transaction test.");
    }

    private List<Long> insertData() throws Exception {
        ShardingKit.print("Insert Data");
        List<Long> result = Lists.newArrayListWithCapacity(10);
        for (int i = 1; i <= 10; i++) {
            User user = new User();
            user.setUserId(i);
            user.setUserName("test_" + i);
            user.setPwd("pwd" + i);
            userRepository.insert(user);
            result.add(user.getUserId());
        }
        return result;
    }

    private void deleteData(final List<Long> userIds) throws Exception {
        ShardingKit.print("Delete Data");
        for (Long each : userIds) {
            userRepository.delete(each);
        }
    }

    @Override
    public void printData() throws Exception {
        ShardingKit.print("Print User Data");
        userRepository.selectAll().forEach(System.out::println);
    }
}
