package top.wugy.sharding.api;

import javax.sql.DataSource;

/**
 * wugy 2021/10/17 17:31
 */
public interface ExampleDataSource {

    DataSource getDataSource() throws Exception;
}
