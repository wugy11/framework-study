package top.wugy.sharding.api.impl.raw;

import top.wugy.sharding.api.ExampleService;
import top.wugy.sharding.entity.ShadowUser;
import top.wugy.sharding.kit.ShardingKit;
import top.wugy.sharding.repository.ShadowUserRepository;

import java.util.ArrayList;
import java.util.List;

public final class ShadowUserServiceImpl implements ExampleService {
    
    private final ShadowUserRepository userRepository;
    
    public ShadowUserServiceImpl(final ShadowUserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @Override
    public void initEnv() throws Exception {
        userRepository.createTableIfNotExist();
        userRepository.truncateTable();
    }
    
    @Override
    public void cleanEnv() throws Exception {
        userRepository.dropTable();
    }
    
    @Override
    public void processSuccess() throws Exception {
        ShardingKit.print("Process Success Begin");
        List<Long> userIds = insertData();
        printData();
        deleteData(userIds);
        printData();
        ShardingKit.print("Process Success Finish");
    }
    
    @Override
    public void processFailure() throws Exception {
        ShardingKit.print("Process Failure Begin");
        insertData();
        ShardingKit.print("Process Failure Finish");
        throw new RuntimeException("Exception occur for transaction test.");
    }
    
    private List<Long> insertData() throws Exception {
        ShardingKit.print("Insert Data");
        List<Long> result = new ArrayList<>(10);
        for (int i = 1; i <= 10; i++) {
            ShadowUser user = new ShadowUser();
            user.setUserId(i);
            user.setUserType(i % 2);
            user.setUserName("test_" + i);
            user.setPwd("pwd" + i);
            userRepository.insert(user);
            result.add((long) user.getUserId());
        }
        return result;
    }
    
    private void deleteData(final List<Long> userIds) throws Exception {
        ShardingKit.print("Delete Data");
        for (Long each : userIds) {
            userRepository.delete(each);
        }
    }
    
    @Override
    public void printData() throws Exception {
        ShardingKit.print("Print User Data");
        for (Object each : userRepository.selectAll()) {
            System.out.println(each);
        }
    }
}
