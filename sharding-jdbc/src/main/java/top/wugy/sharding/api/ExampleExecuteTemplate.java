package top.wugy.sharding.api;

/**
 * wugy 2021/10/17 18:43
 */
public class ExampleExecuteTemplate {

    public static void run(final ExampleService exampleService) throws Exception {
        try {
            exampleService.initEnv();
            exampleService.processSuccess();
        } finally {
            exampleService.cleanEnv();
        }
    }
}
