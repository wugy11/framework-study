package top.wugy.sharding.api.impl.spring;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import top.wugy.sharding.api.ExampleService;
import top.wugy.sharding.entity.Address;
import top.wugy.sharding.entity.Order;
import top.wugy.sharding.entity.OrderItem;
import top.wugy.sharding.kit.ShardingKit;
import top.wugy.sharding.repository.AddressRepository;
import top.wugy.sharding.repository.OrderItemRepository;
import top.wugy.sharding.repository.OrderRepository;

import java.util.List;

/**
 * wugy 2021/10/17 20:29
 */
@Primary
@Service
public class OrderServiceImpl implements ExampleService {

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, OrderItemRepository orderItemRepository,
                            AddressRepository addressRepository) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    @Transactional
    public void initEnv() throws Exception {
        orderRepository.createTableIfNotExist();
        orderItemRepository.createTableIfNotExist();
        orderRepository.truncateTable();
        orderItemRepository.truncateTable();

        initAddressTable();
    }

    private void initAddressTable() throws Exception {
        addressRepository.createTableIfNotExist();
        addressRepository.truncateTable();
        initAddressData();
    }

    private void initAddressData() throws Exception {
        for (int i = 0; i < 10; i++) {
            Address address = new Address();
            address.setAddressId(i);
            address.setAddressName("address_" + i);
            addressRepository.insert(address);
        }
    }

    @Override
    @Transactional
    public void cleanEnv() throws Exception {
        orderRepository.dropTable();
        orderItemRepository.dropTable();
        addressRepository.dropTable();
    }

    @Override
    @Transactional
    public void processSuccess() throws Exception {
        ShardingKit.print("Process Success Begin");
        List<Long> orderIds = insertData();
        printData();
        deleteData(orderIds);
        printData();
        ShardingKit.print("Process Success Finish");
    }

    @Override
    @Transactional
    public void processFailure() throws Exception {
        ShardingKit.print("Process Failure Begin");
        insertData();
        ShardingKit.print("Process Failure Finish");
        throw new RuntimeException("Exception occur for transaction test.");
    }

    private List<Long> insertData() throws Exception {
        ShardingKit.print("Insert Data");
        List<Long> result = Lists.newArrayListWithCapacity(10);
        for (int i = 1; i <= 10; i++) {
            Order order = insertOrder(i);
            insertOrderItem(i, order);
            result.add(order.getOrderId());
        }
        return result;
    }

    private Order insertOrder(final int i) throws Exception {
        Order order = new Order();
        order.setUserId(i);
        order.setAddressId(i);
        order.setStatus("INSERT_TEST");
        orderRepository.insert(order);
        return order;
    }

    private void insertOrderItem(final int i, final Order order) throws Exception {
        OrderItem item = new OrderItem();
        item.setOrderId(order.getOrderId());
        item.setUserId(i);
        item.setStatus("INSERT_TEST");
        orderItemRepository.insert(item);
    }

    private void deleteData(final List<Long> orderIds) throws Exception {
        ShardingKit.print("Delete Data");
        for (Long each : orderIds) {
            orderRepository.delete(each);
            orderItemRepository.delete(each);
        }
    }

    @Override
    public void printData() throws Exception {
        ShardingKit.print("Print Order Data");
        orderRepository.selectAll().forEach(System.out::println);

        ShardingKit.print("Print OrderItem Data");
        orderItemRepository.selectAll().forEach(System.out::println);
    }
}
