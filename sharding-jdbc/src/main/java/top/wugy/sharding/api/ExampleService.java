package top.wugy.sharding.api;

/**
 * wugy 2021/10/17 18:20
 */
public interface ExampleService {

    void initEnv() throws Exception;

    void cleanEnv() throws Exception;

    void processSuccess() throws Exception;

    void processFailure() throws Exception;

    void printData() throws Exception;
}
