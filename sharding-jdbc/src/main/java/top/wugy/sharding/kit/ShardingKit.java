package top.wugy.sharding.kit;

import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.File;
import java.util.Objects;

/**
 * mysql版本：8.0.27
 *
 * wugy 2021/10/17 18:00
 */
public class ShardingKit {

    private static final String HOST = "localhost";

    private static final int PORT = 3306;

    private static final String USER_NAME = "root";

    private static final String PASSWORD = "wugy";

    public static DataSource createDataSource(String dataSourceName) {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(com.mysql.cj.jdbc.Driver.class.getName());
        hikariDataSource.setJdbcUrl(String.format("jdbc:mysql://%s:%s/%s?serverTimezone=UTC&useSSL=false&useUnicode=true&characterEncoding=UTF-8&allowPublicKeyRetrieval=true",
                HOST, PORT, dataSourceName));
        hikariDataSource.setUsername(USER_NAME);
        hikariDataSource.setPassword(PASSWORD);
        return hikariDataSource;
    }

    public static File getYamlFile(String fileName) {
        return new File(Objects.requireNonNull(ShardingKit.class.getResource(fileName)).getFile());
    }

    public static void print(String msg) {
        System.out.printf("---------------------------- %s ----------------------------%n", msg);
    }
}
