package top.wugy.sharding;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.transaction.jta.JtaAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import top.wugy.sharding.api.ExampleExecuteTemplate;
import top.wugy.sharding.api.ExampleService;

@MapperScan(basePackages = "top.wugy.sharding.repository.mybatis")
@SpringBootApplication(exclude = {JtaAutoConfiguration.class})
public class ShardingSpringBootTest {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = SpringApplication.run(ShardingSpringBootTest.class, args);
        ExampleExecuteTemplate.run(context.getBean(ExampleService.class));
    }
}
