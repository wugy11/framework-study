package top.wugy.sharding.algorithm;

import com.google.common.collect.Range;
import org.apache.shardingsphere.sharding.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.sharding.api.sharding.standard.RangeShardingValue;
import org.apache.shardingsphere.sharding.api.sharding.standard.StandardShardingAlgorithm;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * wugy 2021/10/23 19:29
 */
public class StandardModuloShardingTableAlgorithm implements StandardShardingAlgorithm<Long> {

    @Override
    public String doSharding(Collection<String> tableNames, PreciseShardingValue<Long> shardingValue) {
        return tableNames.stream().filter(each -> each.endsWith(String.valueOf(shardingValue.getValue() % 2))).findFirst()
                .orElseThrow();
    }

    @Override
    public Collection<String> doSharding(Collection<String> tableNames, RangeShardingValue<Long> shardingValue) {
        if (Range.closed(200000000000000000L, 400000000000000000L).encloses(shardingValue.getValueRange())) {
            Set<String> result = new LinkedHashSet<>();
            for (String each : tableNames) {
                if (each.endsWith("0")) {
                    result.add(each);
                }
            }
            return result;
        }
        throw new UnsupportedOperationException("");
    }

    @Override
    public void init() {

    }

    @Override
    public String getType() {
        return "standard_test_tbl";
    }
}
