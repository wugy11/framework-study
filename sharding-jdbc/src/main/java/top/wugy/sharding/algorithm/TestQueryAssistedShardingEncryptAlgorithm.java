package top.wugy.sharding.algorithm;

import org.apache.shardingsphere.encrypt.spi.QueryAssistedEncryptAlgorithm;
import lombok.Getter;
import lombok.Setter;
import java.util.Properties;

public final class TestQueryAssistedShardingEncryptAlgorithm implements QueryAssistedEncryptAlgorithm {

    @Getter
    @Setter
    private Properties props;

    @Override
    public void init() {
    }
    
    @Override
    public String encrypt(final Object plaintext) {
        return "encryptValue";
    }
    
    @Override
    public Object decrypt(final String ciphertext) {
        return "decryptValue";
    }
    
    @Override
    public String queryAssistedEncrypt(final Object plaintext) {
        return "assistedEncryptValue";
    }
    
    @Override
    public String getType() {
        return "assistedTest";
    }
}
