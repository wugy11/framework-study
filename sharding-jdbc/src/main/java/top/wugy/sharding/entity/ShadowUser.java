package top.wugy.sharding.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * wugy 2022/2/12 8:57 上午
 */
@Setter
@Getter
public class ShadowUser {

    private int userId;

    private int userType;

    private String userName;

    private String userNamePlain;

    private String pwd;

    private String assistedQueryPwd;
}
