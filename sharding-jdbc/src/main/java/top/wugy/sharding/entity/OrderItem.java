package top.wugy.sharding.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * wugy 2021/10/17 18:38
 */
@Getter
@Setter
public class OrderItem implements Serializable {

    private long orderItemId;

    private long orderId;

    private int userId;

    private String status;

    @Override
    public String toString() {
        return String.format("order_item_id:%s, order_id: %s, user_id: %s, status: %s", orderItemId, orderId, userId, status);
    }
}
