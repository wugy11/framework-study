package top.wugy.sharding.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * wugy 2021/10/17 18:38
 */
@Setter
@Getter
public class User implements Serializable {

    private long userId;

    private String userName;

    private String userNamePlain;

    private String pwd;

    private String assistedQueryPwd;

    @Override
    public String toString() {
        return String.format("user_id: %d, user_name: %s, user_name_plain: %s, pwd: %s, assisted_query_pwd: %s",
                userId, userName, userNamePlain, pwd, assistedQueryPwd);
    }
}
