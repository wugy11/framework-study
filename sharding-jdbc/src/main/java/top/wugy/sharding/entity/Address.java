package top.wugy.sharding.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * wugy 2021/10/17 18:37
 */
@Setter
@Getter
public class Address {

    private long addressId;

    private String addressName;
}
