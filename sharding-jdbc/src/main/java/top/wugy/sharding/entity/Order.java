package top.wugy.sharding.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * wugy 2021/10/17 18:35
 */
@Getter
@Setter
public class Order implements Serializable {

    private long orderId;

    private int userId;

    private long addressId;

    private String status;

    @Override
    public String toString() {
        return String.format("order_id: %s, user_id: %s, address_id: %s, status: %s", orderId, userId, addressId, status);
    }
}
