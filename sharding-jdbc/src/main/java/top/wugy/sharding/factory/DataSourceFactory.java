package top.wugy.sharding.factory;

import top.wugy.sharding.config.*;
import top.wugy.sharding.config.shadow.ShadowConfiguration;
import top.wugy.sharding.kit.ShardingType;

import javax.sql.DataSource;

/**
 * wugy 2021/10/17 17:26
 */
public class DataSourceFactory {

    public static DataSource newInstance(ShardingType shardingType) throws Exception {
        switch (shardingType) {
            case SHARDING_DATABASES:
                return new ShardingDatabasesConfigurationPrecise().getDataSource();
            case SHARDING_TABLES:
                return new ShardingTablesConfigurationPrecise().getDataSource();
            case SHARDING_DATABASES_AND_TABLES:
                return new ShardingDatabasesAndTablesConfigurationPrecise().getDataSource();
            case READWRITE_SPLITTING:
                return new ReadwriteSplittingConfiguration().getDataSource();
            case SHADOW:
                return new ShadowConfiguration().getDataSource();
        }
        throw new UnsupportedOperationException(String.format("%s is unknown", shardingType));
    }
}
