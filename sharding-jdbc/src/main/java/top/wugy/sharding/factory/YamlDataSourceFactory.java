package top.wugy.sharding.factory;

import org.apache.shardingsphere.driver.api.yaml.YamlShardingSphereDataSourceFactory;
import top.wugy.sharding.kit.ShardingType;

import javax.sql.DataSource;

import static top.wugy.sharding.kit.ShardingKit.getYamlFile;

/**
 * wugy 2021/10/24 17:05
 */
public class YamlDataSourceFactory {

    public static DataSource newInstance(ShardingType shardingType) throws Exception {
        switch (shardingType) {
            case SHARDING_DATABASES:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/yaml/sharding-database.yaml"));
            case SHARDING_TABLES:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/yaml/sharding-table.yaml"));
            case SHARDING_DATABASES_AND_TABLES:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/yaml/sharding-database-table.yaml"));
            case READWRITE_SPLITTING:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/yaml/readwrite-splitting.yaml"));
//            case SHARDING_MASTER_SLAVE:
//                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/yaml/sharding-readwrite-splitting.yaml"));
            case HINT_DATABASE_ONLY:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/hint/hint-database-only.yaml"));
            case HINT_DATABASE_TABLE:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/hint/hint-database-table.yaml"));
            case HINT_SQL_COMMENT:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/hint/hint-sql-comment.yaml"));
            case HINT_MASTER_SALVE:
                return YamlShardingSphereDataSourceFactory.createDataSource(getYamlFile("/META-INF/hint/hint-master-slave.yaml"));
        }
        throw new UnsupportedOperationException(String.format("%s is unknown", shardingType));
    }

}
