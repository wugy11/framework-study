drop table if exists t_product;
CREATE TABLE `t_product` (
 `id` bigint unsigned NOT NULL AUTO_INCREMENT,
 `num` bigint unsigned NOT NULL,
 `version` bigint unsigned NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO t_product(id, num, version) VALUES(1, 100, 0);

drop table if exists t_user;
CREATE TABLE t_user (
    id BIGINT(20) NOT NULL COMMENT '主键ID',
    tenant_id BIGINT(20) NOT NULL COMMENT '租户ID',
    name VARCHAR(30) NULL DEFAULT NULL COMMENT '姓名',
    PRIMARY KEY (id)
);
INSERT INTO t_user (id, tenant_id, name) VALUES
(1, 1, 'Jone'),(2, 1, 'Jack'),(3, 1, 'Tom'),
(4, 0, 'Sandy'),(5, 0, 'Billie');

DROP TABLE IF EXISTS t_user_addr;
CREATE TABLE t_user_addr (
    id BIGINT(20) NOT NULL COMMENT '主键ID',
    user_id BIGINT(20) NOT NULL COMMENT 'user.id',
    name VARCHAR(30) NULL DEFAULT NULL COMMENT '地址名称',
    PRIMARY KEY (id)
);

INSERT INTO t_user_addr (id, USER_ID, name) VALUES
(1, 1, 'addr1'), (2, 1, 'addr2');