package top.wugy.cloud.feign.provider.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * wugy 2022/3/18 17:15
 */
@Getter
@Setter
@TableName("t_account")
public class AccountEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private int num;
}
