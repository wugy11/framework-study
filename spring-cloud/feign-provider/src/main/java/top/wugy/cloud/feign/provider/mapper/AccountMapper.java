package top.wugy.cloud.feign.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.wugy.cloud.feign.provider.entity.AccountEntity;

/**
 * wugy 2022/3/18 17:14
 */
@Mapper
public interface AccountMapper extends BaseMapper<AccountEntity> {
}
