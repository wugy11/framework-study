package top.wugy.cloud.feign.provider.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;

import java.io.Serializable;

/**
 * wugy 2021/12/25 15:10
 */
@Data
@TableName("t_product")
public class ProductEntity implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long num;
    @Version
    private Integer version;
}
