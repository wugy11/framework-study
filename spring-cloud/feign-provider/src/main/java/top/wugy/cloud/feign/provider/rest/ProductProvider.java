package top.wugy.cloud.feign.provider.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.wugy.cloud.comm.dto.ProductDto;
import top.wugy.cloud.feign.provider.entity.ProductEntity;
import top.wugy.cloud.feign.provider.mapper.ProductMapper;

import javax.annotation.Resource;

/**
 * wugy 2022/3/19 14:11
 */
@RestController
@RequestMapping("/product")
public class ProductProvider {

    @Resource
    private ProductMapper productMapper;

    @PostMapping(value = "/deductProductStore")
    public int deductProductStore(@RequestBody ProductDto productDto) {
        ProductEntity productEntity = new ProductEntity();
        productEntity.setId((long) productDto.getId());
        productEntity.setNum((long) productDto.getProductNum());
        return productMapper.updateById(productEntity);
    }
}
