package top.wugy.cloud.feign.provider.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.wugy.cloud.comm.dto.AccountDto;
import top.wugy.cloud.feign.provider.entity.AccountEntity;
import top.wugy.cloud.feign.provider.mapper.AccountMapper;

import javax.annotation.Resource;

/**
 * wugy 2022/3/19 14:07
 */
@RestController
@RequestMapping("/account")
public class AccountProvider {

    @Resource
    private AccountMapper accountMapper;

    @PostMapping(value = "/deductAccountBalance")
    public int deductAccountBalance(@RequestBody AccountDto accountDto) {
        AccountEntity entity = new AccountEntity();
        entity.setId(accountDto.getId());
        entity.setNum(accountDto.getAccountNum());
        return accountMapper.updateById(entity);
    }
}
