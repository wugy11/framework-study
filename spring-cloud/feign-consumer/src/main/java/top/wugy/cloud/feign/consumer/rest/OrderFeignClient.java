package top.wugy.cloud.feign.consumer.rest;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import top.wugy.cloud.comm.dto.AccountDto;
import top.wugy.cloud.comm.dto.ProductDto;

@FeignClient(name = "cloud-feign-provider")
public interface OrderFeignClient {

    @PostMapping(value = "/account/deductAccountBalance")
    int deductAccountBalance(AccountDto accountDto);

    @PostMapping(value = "/product/deductProductStore")
    int deductProductStore(ProductDto ProductDto);
}