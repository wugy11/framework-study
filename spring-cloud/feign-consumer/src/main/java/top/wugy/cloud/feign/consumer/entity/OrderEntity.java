package top.wugy.cloud.feign.consumer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * wugy 2022/3/19 14:18
 */
@Getter
@Setter
@TableName("t_order")
public class OrderEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private int num;

    private Date createTime;
}
