package top.wugy.cloud.feign.consumer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.wugy.cloud.feign.consumer.entity.OrderEntity;

/**
 * wugy 2022/3/19 14:18
 */
@Mapper
public interface OrderMapper extends BaseMapper<OrderEntity> {
}
