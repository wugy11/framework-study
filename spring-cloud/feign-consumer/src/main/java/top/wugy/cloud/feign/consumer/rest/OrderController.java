package top.wugy.cloud.feign.consumer.rest;

import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import top.wugy.cloud.comm.dto.AccountDto;
import top.wugy.cloud.comm.dto.ProductDto;
import top.wugy.cloud.feign.consumer.entity.OrderEntity;
import top.wugy.cloud.feign.consumer.mapper.OrderMapper;

import javax.annotation.Resource;
import java.util.Date;

/**
 * wugy 2022/3/19 14:20
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderFeignClient orderFeignConsumer;
    @Resource
    private OrderMapper orderMapper;

    @RequestMapping(value = "/buy/{productId}", method = RequestMethod.POST)
    @GlobalTransactional(name = "buyGood", rollbackFor = Exception.class)
    public int buy(@PathVariable("productId") Integer productId) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setNum(1);
        orderEntity.setCreateTime(new Date());
        orderMapper.insert(orderEntity);

        orderFeignConsumer.deductProductStore(new ProductDto(productId, 1));
        orderFeignConsumer.deductAccountBalance(new AccountDto(1, 1));
        return 1;
    }
}
