package top.wugy.cloud.dubbo.provider.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.wugy.dubbo.api.RibbonDiscoveryService;

import javax.annotation.Resource;

@RestController
@RequestMapping("/provider")
public class ProviderController {

    @Resource
    private RibbonDiscoveryService ribbonDiscoveryService;

    @GetMapping(value = "/getRibbonConfig")
    public String testRestRibbon(){
        return ribbonDiscoveryService.getRibbonConfig();
    }
}
