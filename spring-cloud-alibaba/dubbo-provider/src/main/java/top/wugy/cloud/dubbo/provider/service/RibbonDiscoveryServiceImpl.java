package top.wugy.cloud.dubbo.provider.service;

import lombok.SneakyThrows;
import org.apache.dubbo.config.annotation.DubboService;
import top.wugy.dubbo.api.RibbonDiscoveryService;

import java.net.InetAddress;

@DubboService(group = "ribbon-provider")
public class RibbonDiscoveryServiceImpl implements RibbonDiscoveryService {

    @SneakyThrows
    @Override
    public String getRibbonConfig() {
        return String.format("当前ip：%s", InetAddress.getLocalHost().toString());
    }
}
