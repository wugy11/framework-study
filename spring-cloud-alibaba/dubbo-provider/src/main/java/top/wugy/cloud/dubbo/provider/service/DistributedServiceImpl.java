package top.wugy.cloud.dubbo.provider.service;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.apache.dubbo.config.annotation.DubboService;
import top.wugy.dubbo.api.DistributedService;

/**
 * wugy 2022/1/16 7:18 下午
 */
@DubboService(group = "distributed-id-provider")
public class DistributedServiceImpl implements DistributedService {

    @Override
    public long nextId() {
        return IdWorker.getId();
    }

    @Override
    public long nextId(long datacenterId, long machineId) {
        IdWorker.initSequence(machineId, datacenterId);
        return IdWorker.getId();
    }
}
