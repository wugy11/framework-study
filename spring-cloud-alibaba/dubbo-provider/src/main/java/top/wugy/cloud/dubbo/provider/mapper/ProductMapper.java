package top.wugy.cloud.dubbo.provider.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.wugy.cloud.dubbo.provider.entity.ProductEntity;

/**
 * wugy 2021/12/25 15:14
 */
@Mapper
public interface ProductMapper extends BaseMapper<ProductEntity> {
}
