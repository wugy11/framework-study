package top.wugy.cloud.dubbo.provider.service;

import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import top.wugy.cloud.dubbo.provider.entity.ProductEntity;
import top.wugy.cloud.dubbo.provider.mapper.ProductMapper;
import top.wugy.dubbo.api.ProductService;
import top.wugy.dubbo.api.request.ProductRequest;
import top.wugy.dubbo.api.response.ProductResponse;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@DubboService(group = "product-provider")
public class ProductServiceImpl implements ProductService {

    @Resource
    private RedisTemplate<String, Boolean> redisTemplate;
    @Resource
    private ProductMapper productMapper;

    @Override
    public ProductResponse updateProductNum(ProductRequest request) {
        ProductResponse response = new ProductResponse();
        String uuid = request.getUuid();
        if (StringUtils.hasLength(uuid)) {
            if (Objects.nonNull(redisTemplate.opsForValue().get(uuid))) {
                response.setSuccess(true);
                response.setRetMsg(String.format("uuid:%s已经连续多次访问！", uuid));
                return response;
            }
            redisTemplate.opsForValue().set(uuid, true, 2, TimeUnit.SECONDS);
        }
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Long id = request.getId();
        ProductEntity productEntity = productMapper.selectById(id);
        Long num = productEntity.getNum();
        System.out.printf("开始增加库存，产品ID=%d，增加前库存为：%d%n", id, num);

        ProductEntity updateEntity = new ProductEntity();
        updateEntity.setId(id);
        updateEntity.setNum(num + request.getAddNum());
        productMapper.updateById(updateEntity);

        productEntity = productMapper.selectById(id);
        num = productEntity.getNum();
        System.out.printf("增加库存后，产品ID=%d，库存为：%d%n", id, num);

        response.setSuccess(true);
        response.setId(id);
        response.setNum(num);
        return response;
    }
}
