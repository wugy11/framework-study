package top.wugy.dubbo.api.response;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;


@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
public class ProductResponse extends BaseResponse {

    private Long id;

    private Long num;
}
