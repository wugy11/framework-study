package top.wugy.dubbo.api.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Setter
@Getter
@Accessors(chain = true)
public class ProductRequest implements Serializable {

    private String uuid;

    private Long id;

    private Long addNum;
}
