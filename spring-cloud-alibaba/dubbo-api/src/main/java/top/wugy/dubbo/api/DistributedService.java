package top.wugy.dubbo.api;

/**
 * wugy 2022/1/16 7:17 下午
 */
public interface DistributedService {

    long nextId();

    long nextId(long datacenterId, long machineId);
}
