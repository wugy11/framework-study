package top.wugy.dubbo.api;

import top.wugy.dubbo.api.request.ProductRequest;
import top.wugy.dubbo.api.response.ProductResponse;

public interface ProductService {

    /**
     * 修改产品库存
     */
    ProductResponse updateProductNum(ProductRequest request);
}
