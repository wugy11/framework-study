package top.wugy.cloud.dubbo.consumer.rest;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import top.wugy.cloud.dubbo.consumer.config.NacosConfig;
import top.wugy.dubbo.api.DistributedService;
import top.wugy.dubbo.api.ProductService;
import top.wugy.dubbo.api.RibbonDiscoveryService;
import top.wugy.dubbo.api.request.ProductRequest;
import top.wugy.dubbo.api.response.ProductResponse;

import javax.annotation.Resource;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    @DubboReference(group = "ribbon-provider", timeout = 8000, check = false)
    private RibbonDiscoveryService ribbonDiscoveryService;
    @DubboReference(group = "product-provider", timeout = 1, retries = 4)
    private ProductService productService;
    @DubboReference(group = "distributed-id-provider")
    private DistributedService distributedService;

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private NacosConfig nacosConfig;

    @GetMapping("/testDubboRibbonConsumer")
    public void testDubboRibbonConsumer() {
        System.out.println(ribbonDiscoveryService.getRibbonConfig());
        System.out.println(restTemplate.getForObject("http://alibaba-cloud-dubbo-provider/provider/getRibbonConfig", String.class));
    }

    @GetMapping("/testUpdateProductNum/{id}")
    public ProductResponse testUpdateProductNum(@PathVariable("id") Long id) {
        ProductRequest request = new ProductRequest().setId(id).setAddNum(10L);
        if (nacosConfig.isSwitchOn()) {
            request.setUuid(String.valueOf(distributedService.nextId()));
        }
        return productService.updateProductNum(request);
    }
}
