package top.wugy.cloud.dubbo.consumer.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
@Getter
@Setter
public class NacosConfig {

    @Value("${switchOn}")
    private boolean switchOn;
}
